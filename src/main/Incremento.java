package main;

public class Incremento {
    private int total = 0; // el total de todos los incrementos
    private final int INCREMENTO; // variable constante (sin inicializar)
    /** El constructor inicializa la variable de instancia ﬁnal INCREMENTO **/
    public Incremento( int valorIncremento )
    {
        INCREMENTO = valorIncremento; // inicializa la variable constante (una vez)
    }
    /** Suma INCREMENTO al total **/
    public void sumarIncrementoATotal()
    {
        total += INCREMENTO;
    }

    public String toString()
    {
        return String.format( "total = %d", total );
    }
}
